#!/bin/bash
# autopkgtest check: Check plugin command using asdf tool.
# Author: Arthur Diniz <arthurbdiniz@gmail.com>

. debian/tests/utils.sh

asdf_list_plugin_output=($(asdf plugin list))
len_output=(${#asdf_list_plugin_output[@]})

if [[ $len_output -ne 1 ]]; then
    exit 1
fi

if [[ "$asdf_list_plugin_output" != "dummy" ]]; then
    exit 1
fi

echo "asdf list plugin: OK"

asdf plugin-remove dummy 2>&1 > /dev/null

# By default if the plugin does not exits a override
# is required to set the exit code to 0
asdf_remove_plugin_output=$(asdf plugin-remove dummy 2>&1 || :)

if [[ $asdf_remove_plugin_output != "No such plugin: dummy" ]]; then
    exit 1
fi

echo "asdf remove plugin: OK"
