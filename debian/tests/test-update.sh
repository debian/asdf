#!/bin/bash
# autopkgtest check: Check update command using asdf tool.
# Author: Arthur Diniz <arthurbdiniz@gmail.com>

. debian/tests/utils.sh

# By default if the plugin does not exits a override
# is required to set the exit code to 0
asdf_update_output=$(asdf update 2>&1 || :)

if [[ "$asdf_update_output" != "Update command disabled. Please use the package manager that you used to install asdf to upgrade asdf." ]]; then
    exit 1
fi

echo "asdf update: OK"
