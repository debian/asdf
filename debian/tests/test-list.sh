#!/bin/bash
# autopkgtest check: Check list command using asdf tool.
# Author: Arthur Diniz <arthurbdiniz@gmail.com>

. debian/tests/utils.sh

asdf_list_all_dummy_output_list=($(asdf list all dummy))
len_output=(${#asdf_list_all_dummy_output_list[@]})

if [[ $len_output -ne 3 ]]; then
    exit 1
fi

echo "asdf list all: OK"
