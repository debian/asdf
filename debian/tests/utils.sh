set -euo pipefail

if [ -z "$AUTOPKGTEST_TMP" ]; then
    AUTOPKGTEST_TMP=$(mktemp -d)
    trap 'rm -rf ${AUTOPKGTEST_TMP}' EXIT
fi

export ASDF_DATA_DIR=$AUTOPKGTEST_TMP
export HOME=$AUTOPKGTEST_TMP

source /etc/profile.d/asdf.sh

mkdir -p "$ASDF_DATA_DIR/plugins"
cp -r "test/fixtures/dummy_plugin" "$ASDF_DATA_DIR/plugins/dummy"

# Cleanup
asdf uninstall dummy 2>&1 >/dev/null

if [[ -f ".tool-versions" ]]; then
    rm .tool-versions
fi

if [[ -f "$HOME/.tool-versions" ]]; then
    rm $HOME/.tool-versions
fi
