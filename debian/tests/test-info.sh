#!/bin/bash
# autopkgtest check: Check info command using asdf tool.
# Author: Arthur Diniz <arthurbdiniz@gmail.com>

. debian/tests/utils.sh

asdf info >/dev/null

echo "asdf info: OK"
