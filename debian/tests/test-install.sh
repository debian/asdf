#!/bin/bash
# autopkgtest check: Check install, local and global commands using asdf tool.
# Author: Arthur Diniz <arthurbdiniz@gmail.com>

. debian/tests/utils.sh

asdf install dummy 1.0.0
asdf install dummy 2.0.0

asdf_list_dummy_output_list=($(asdf list dummy))
len_output=(${#asdf_list_dummy_output_list[@]})

if [[ $len_output -ne 2 ]]; then
    exit 1
fi

if [[ ${asdf_list_dummy_output_list[0]} != "1.0.0" ]]; then
    exit 1
fi

if [[ ${asdf_list_dummy_output_list[1]} != "2.0.0" ]]; then
    exit 1
fi

echo "asdf install dummy: OK"

asdf local dummy 2.0.0

echo $PATH
dummy | grep "This is Dummy 2.0.0!"

# dummy_output=$(dummy)

# if [[ $dummy_output != "This is Dummy 2.0.0!" ]]; then
#     exit 1
# fi

echo "dummy command: OK"

if [ ! -e ".tool-versions" ]; then
    exit 1
fi

tool_versions_output=$(cat .tool-versions)

if [[ $tool_versions_output != "dummy 2.0.0" ]]; then
    exit 1
fi

echo "asdf local: OK"

rm .tool-versions

asdf global dummy 2.0.0

if [ ! -e "$AUTOPKGTEST_TMP/.tool-versions" ]; then
    exit 1
fi

tool_versions_output=$(cat $AUTOPKGTEST_TMP/.tool-versions)

if [[ $tool_versions_output != "dummy 2.0.0" ]]; then
    exit 1
fi


echo "asdf global: OK"
