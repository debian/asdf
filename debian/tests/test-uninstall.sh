#!/bin/bash
# autopkgtest check: Check uninstall command using asdf tool.
# Author: Arthur Diniz <arthurbdiniz@gmail.com>

. debian/tests/utils.sh

asdf install dummy latest
asdf uninstall dummy

asdf_list_dummy_output=$(asdf list dummy 2>&1 || :)
trimmed_asdf_list_dummy_output="$(echo "$asdf_list_dummy_output" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')"

if [[ "$trimmed_asdf_list_dummy_output" != "No versions installed" ]]; then
    exit 1
fi

echo "asdf uninstall dummy: OK"
