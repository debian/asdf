#!/bin/bash
# autopkgtest check: Check latest command using asdf tool.
# Author: Arthur Diniz <arthurbdiniz@gmail.com>

. debian/tests/utils.sh

asdf_latest_dummy_output=($(asdf latest dummy))
if [[ "$asdf_latest_dummy_output" != "2.0.0" ]]; then
    exit 1
fi
echo "asdf latest dummy: OK"
